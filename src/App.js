import React, { useState } from "react";
import axios from "axios";
import Loader from "react-loader-spinner";
import { DropzoneArea } from "material-ui-dropzone";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import "./App.css";
import logo from "./logo.png";

function App() {
  const [contenuFile, setContenuFile] = useState();
  const [styleFile, setStyleFile] = useState();
  const [defaultStyle, setDefaultStyle] = useState({
    lib: "none",
    relativ_path: "none",
  });
  const [epochs, setEpochs] = useState(25);
  const [etapesEpochs, setEtapesEpochs] = useState(25);
  const [poidsContenu, setPoidsContenu] = useState(5);
  const [poidsStyle, setPoidsStyle] = useState(5);
  const [name, setName] = useState("");
  const [sleep, setSleep] = useState(false);

  const handleContenuChange = (event) => {
    setContenuFile(event[0]);
  };

  const handleStyleChange = (event) => {
    setStyleFile(event[0]);
  };

  const handleDefaultStyleChange = (event) => {
    setDefaultStyle({
      lib: event.target.value.split(".")[0],
      relativ_path: event.target.value,
    });
  };

  const handleEpochChange = (event, value) => {
    setEpochs(value);
  };

  const handleEpochStepChange = (event, value) => {
    setEtapesEpochs(value);
  };

  const handleContenuWeightChange = (event, value) => {
    setPoidsContenu(value);
  };

  const handleStyleWeightChange = (event, value) => {
    setPoidsStyle(value);
  };

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleProcessClick = (event) => {
    const contenu = new FormData();
    const style = new FormData();
    contenu.append("file", contenuFile);
    style.append("file", styleFile);

    const posts = [];
    posts.push(
      axios.post("http://localhost:3001/upload?type_img=contenu", contenu, {
        headers: { "Content-type": "multipart/form-data" },
      })
    );
    if (defaultStyle.lib === "none") {
      posts.push(
        axios.post("http://localhost:3001/upload?type_img=style", style, {
          headers: { "Content-type": "multipart/form-data" },
        })
      );
    }

    Promise.all(posts).then((result) => {
      setSleep(true);
      axios
        .post("http://localhost:3001/images", {
          contenuName: result[0].data.path,
          styleName:
            defaultStyle.relativ_path === "none"
              ? result[1].data.path
              : defaultStyle.relativ_path,
          output: `${name}.png`,
          epochs,
          etapes_epochs: etapesEpochs,
          poids_style: Math.pow(10, poidsStyle - 10),
          poids_contenu: Math.pow(10, poidsContenu),
        })
        .then(() => {
          setSleep(false);
        });
    });
  };

  return (
    <div className="App">
      <header className="App-header">
        <img className="logo" alt="logo" src={logo} />
        <h3>PictArtGenerator</h3>
        <i>
          Réalisé par Louis KREMPP, Guillaume DOCKWILLER, Alexandre BEAUCAMPS et
          Tennessee FEVRIER
        </i>
      </header>
      <div className="Body">
        <div className="intro">
          <Typography id="intro" gutterBottom>
            Avec cet outil vous pouvez appliquer le style d'une peinture sur
            une image
            <br />
            Choisissez juste un contenu en entrée (ex: une photographie)
            <br />
            Puis choisissez un style à appliquer en important une peinture
            <br />
            Enfin, cliquez sur <b>Procéder</b> et voyez le résultat
          </Typography>
        </div>
        <div className="contenu-upload">
          <h3>Choisissez un fichier de contenu</h3>
          <DropzoneArea onChange={handleContenuChange} />
        </div>
        <div className="style-upload">
          <h3>Choisissez un fichier de style</h3>
          <DropzoneArea onChange={handleStyleChange} />
        </div>
        <div className="parameters">
          <Typography>
            Vous pouvez aussi choisir un style prédéfini (cela ne prendra pas en
            compte le fichier de style que vous aurez importé)
          </Typography>
          <FormControl variant="outlined" className="form-control">
            <InputLabel id="demo-simple-select-outlined-label">
              Style prédéfini
            </InputLabel>
            <Select
              labelId="demo-simple-select-outlined-label"
              id="demo-simple-select-outlined"
              value={defaultStyle.relativ_path}
              onChange={handleDefaultStyleChange}
              label="Style prédéfini"
            >
              <MenuItem value="none">
                <em>Ne pas utiliser de prédéfini</em>
              </MenuItem>
              <MenuItem value="Pointillisme.png">Pointillisme</MenuItem>
              <MenuItem value="Abstrait.png">Abstrait</MenuItem>
              <MenuItem value="Brut.png">Brut</MenuItem>
              <MenuItem value="Cubisme.png">Cubisme</MenuItem>
              <MenuItem value="Fauvisme.png">Fauvisme</MenuItem>
              <MenuItem value="hyperrealisme.png">hyperrealisme</MenuItem>
              <MenuItem value="Impressionisme.png">Impressionisme</MenuItem>
              <MenuItem value="Pop.jpg">Pop</MenuItem>
              <MenuItem value="PostImpressionisme.jpg">
                PostImpressionisme
              </MenuItem>
              <MenuItem value="Suprematisme.png">Suprematisme</MenuItem>
              <MenuItem value="Surealisme.png">Surealisme</MenuItem>
              <MenuItem value="Romantisme.png">Romantisme</MenuItem>
              <MenuItem value="Urbain.png">Urbain</MenuItem>
            </Select>
          </FormControl>
          <Typography>
            Vous pouvez également influer sur le nombre de répétitions du
            programme
            <br />
            Sur le nombre d'étapes réalisées par le programme
            <br />
            Et sur l'importance de chaque images dans le résultat
          </Typography>
          <br />
          <br />
          <Typography id="epochs" gutterBottom>
            Nombre de répétitions
          </Typography>
          <Slider
            defaultValue={25}
            aria-labelledby="epochs"
            valueLabelDisplay="auto"
            onChange={handleEpochChange}
            step={5}
            marks
            min={5}
            max={50}
          />
          <Typography id="etapes-epochs" gutterBottom>
            Nombre d'étapes par répétition
          </Typography>
          <Slider
            defaultValue={25}
            aria-labelledby="etapes-epochs"
            valueLabelDisplay="auto"
            onChange={handleEpochStepChange}
            step={5}
            marks
            min={5}
            max={50}
          />
          <Typography id="poids-contenu" gutterBottom>
            Poids du contenu
          </Typography>
          <Slider
            defaultValue={5}
            aria-labelledby="poids-contenu"
            valueLabelDisplay="auto"
            onChange={handleContenuWeightChange}
            marks
            min={1}
            max={10}
          />
          <Typography id="poids-style" gutterBottom>
            Poids du style
          </Typography>
          <Slider
            defaultValue={5}
            aria-labelledby="poids-style"
            valueLabelDisplay="auto"
            onChange={handleStyleWeightChange}
            marks
            min={1}
            max={10}
          />
          <TextField
            id="contenu-name"
            label="Nom"
            variant="outlined"
            onChange={handleNameChange}
          />
        </div>
        <div className="button-upload">
          <Button
            variant="contained"
            color="primary"
            onClick={handleProcessClick}
          >
            Procéder
          </Button>
        </div>
        <div className="result">
          <Typography>Résultat : </Typography>
          <br />
          {sleep ? (
            <Loader
              type="Puff"
              color="#00BFFF"
              height={100}
              width={100}
              timeout={100000000} //3 secs
            />
          ) : (
            <a href={`http://localhost:8080/dataset/generations/${name}.png`}>
              <img
                alt="result"
                src={`http://localhost:8080/dataset/generations/${name}.png?${Date.now()}`}
              />
            </a>
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
